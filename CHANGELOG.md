# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [3.0.0] - 21-02-2020

### Added
* A new attribute **SetCurrentsAverage** to simulate the AC current readings
available on Bilt PS used by FOFB

## [2.2.0] - 07-11-2019

### Changed
* Add a case with 5 PS channels for the two injection specific
SH magnets (sh3 in cell 03 and sh1 in cell 04)

## [2.1.0] - 03-07-2019

### Changed
* Removed the prop used to store currents but locally memorized the last
set values

## [2.0.0] - 01-03-2019

* First tagged release !
