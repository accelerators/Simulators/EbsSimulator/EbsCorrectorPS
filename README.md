# EbsCorrectorPS

A Tango class to be used in simulator for the EBS
correctors PS simulation.

## Cloning

To clone this project, type:

```
git clone git@gitlab.esrf.fr:accelerators/Simulators/EbsSimulator/EbsCorrectorPS.git
```

## Documentation

Pogo generated doc in **doc_html** folder

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies **(Delete If Does Not Apply)**

* Tango Controls 9 or higher.
* omniORB release 4 or higher.
* libzmq - libzmq3-dev or libzmq5-dev.

#### Toolchain Dependencies 

* C++11 compliant compiler.
* CMake 3.0 or greater is required to perform the build.

### Build

Instructions on building the project.

CMake example:

```bash
cd EbsCorrectorPS
mkdir -p build/<os>
cd build/<os>
cmake ../..
make
```
